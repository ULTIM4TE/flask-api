
import sqlite3

from flask import Flask , g
from routes.users import users
from flask_cors import CORS

app = Flask(__name__)

app.config['CORS_HEADERS'] = 'Content-Type'

CORS(app)

DATABASE = './user.db'

def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE,timeout=10)
        db.row_factory = sqlite3.Row 
    return db

def commit_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE,timeout=10)
    return db

@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()

users(app,get_db,commit_db)

if __name__ == '__main__':
    app.run(debug=True)