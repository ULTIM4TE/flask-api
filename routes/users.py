# from flask import Flask, request, jsontify
from flask  import jsonify , request

def users(app,get_db,commit_db):

    def query_db(query, args=(), one=False):
        cur = get_db().execute(query, args)
        rv = cur.fetchall()
        cur.close()
        return (rv[0] if rv else None) if one else rv

    def query_commit_db(query, args=(), one=False):
        conn = commit_db()
        cur = conn.execute(query, args)
        rv = conn.commit()
        return cur.lastrowid

    @app.route('/users')
    def get():
        stateMent = 'SELECT * FROM users'
        if request.args.get('id'):
            stateMent + 'WHERE id = ?' 
            # push
            result = query_db(stateMent,  [ request.args.get('id1') ])
        result = query_db(stateMent)
        # for row in result:
        #     print(dict(row))
        # return result
        # return jsonify(list(result))
        return jsonify([dict(row) for row in result])
        # return { 'error':'You badd' }, 400

    @app.route('/user' , methods = ['POST'])
    def create():
            if request.method == 'POST':
                data = query_db('SELECT id FROM users ORDER BY id DESC LIMIT 1',one=True)

                id = data['id']+1
                first_name = request.json.get("first_name")
                last_name = request.json.get("last_name")
                email = request.json.get("email")
                gender = request.json.get("gender")
                age = request.json.get("age")
            
                user = [id,first_name,last_name,email,gender,age]
                result = query_commit_db('INSERT INTO users (id,first_name,last_name,email,gender,age) values (?,?,?,?,?,?)',user)

            return "Create success !!"
        # return { 'error':'You badd' }, 400

    @app.route('/user/<userId>' , methods = ['PUT','DELETE'])
    def editDelete(userId):
        if request.method == 'PUT':
            "PUT"
        if request.method == 'DELETE':
            "DELETE"
        result = query_db('SELECT * FROM users')
        return jsonify([dict(row) for row in result])
        # return { 'error':'You badd' }, 400